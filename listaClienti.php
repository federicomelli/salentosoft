<!-- Bootstrap -->
<link href="./private/css/bootstrap.min.css" rel="stylesheet">
<script src="./private/js/jquery-3.4.1.min.js"></script>
<script>
$( document ).ready(function() {
  $("")
});
</script>
<?php
require_once './private/database/clienteProvider.php';

$listaClienti = getAllClienti();

//Visualizza il risultato della query sotto forma di tabella
function printTable($listaClienti){
  $result="<table class=\"table table-hover table-bordered\">";
  //header
  $result.="<thead class=\"thead-light\"><tr>
                  <th scope=\"col\">Nome</th>
                  <th scope=\"col\">Cognome</th>
                  <th scope=\"col\">Codice fiscale</th>
                  <th scope=\"col\">Email</th>
                  <th scope=\"col\">Cellulare</th>
                  <th scope=\"col\">Presentatore</th>
                  <th scope=\"col\">Azioni</th>
                  </tr></thead>";
  //body - inizio
  $result.="<tbody>";

  //Mentre una variante interessante è costituita dal fetch_object
  //che permette di gestire il risultato di ritorno come un oggetto:
  while($obj = $listaClienti->fetch_object()){
    $result.="<tr>
                  <td>$obj->nome</td>
                  <td>$obj->cognome</td>
                  <td>$obj->cod_fiscale</td>
                  <td>$obj->email</td>
                  <td>$obj->cellulare</td>
                  <td>$obj->cod_presentatore</td>
                  <td><a href\"#\" id=\"'delete_'.$obj->idcliente\">Delete</a></td>";

  }
  //body & table - fine
  $result.="</tbody></table>";

  return $result;
}
?>
<html>
<head>
    <title>Registra cliente</title>
</head>
<body>
  <div class="container">
    <div class="col-md-12 p-3">
      <div id="table">
        <?php  echo printTable($listaClienti);?>
      </div>
    </div>
  </div>

</body>
</html>
