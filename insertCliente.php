<!-- Bootstrap -->
<link href="./private/css/bootstrap.min.css" rel="stylesheet">
<?php
// require_once './private/database/clienteProvider.php';
//
session_start();

//inizializzo i valori dei campi del form
$codiceFiscale = "";
$nome = "";
$cognome = "";
$email = "";
$cellulare = "";
$presentatore = "";

/*
se in sessione ho un codice fiscale, significa che nel form devo caricare i
*/
if(isset($_SESSION["cfCliente"])){
  $codiceFiscale = $_SESSION["cfCliente"];
  $clienteResult = getClienteByCodiceFiscale($codiceFiscale);
  while ($obj = $clienteResult->fetch_object()) {
    $codiceFiscale = $obj->cod_fiscale;
    $nome = $obj->nome;
    $cognome = $obj->cognome;
    $email = $obj->email;
    $cellulare = $obj->cellulare;
    $presentatore = $obj->cod_presentatore;
  }

}
// if ($_SERVER["REQUEST_METHOD"] == "POST") {
//
//   //flag per controllare l'effettivo successo di un operazione di insert o update
//   $success = false;
//
//   //filter_var - filters a variable with a specified filter
//   $codiceFiscale = filter_var($_POST['codiceFiscale'], FILTER_SANITIZE_STRING);
//   $nome = filter_var($_POST['nome'], FILTER_SANITIZE_STRING);
//   $cognome = filter_var($_POST['cognome'], FILTER_SANITIZE_STRING);
//   $email = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
//   $cellulare = filter_var($_POST['cellulare'], FILTER_SANITIZE_STRING);
//   $presentatore = filter_var($_POST['presentatore'], FILTER_SANITIZE_STRING);
//
//   /*
//   Se il cliente esiste, devo fare un update, altrimenti devo effettuare un insert
//   */
//   if(isset($_SESSION["cfCliente"])){
//     //UPDATE
//     //TODO - FIX NULL PRESENTATORE
//     $success = updateCliente($codiceFiscale, $nome, $cognome, $email, $cellulare, null);
//   }else{
//     //INSERT
//     //TODO - FIX NULL PRESENTATORE
//     $success =insertCliente($codiceFiscale, $nome, $cognome, $email, $cellulare, null);
//   };
//   if($success){
//     //SUCCESSO
//     //rimuovo la variabile di sessione
//     if(isset($_SESSION["cfCliente"])){
//       unset($_SESSION["cfCliente"]);
//     }
//     //redirect alla pagina di visualizzazione dei clienti
//     header("location: listaClienti.php");
//   }else{
//     //ERRORE
//     echo '<div class="alert alert-danger" role="alert">Errore</div>';
//   };
// }

?>

<html>
<head>
    <title>Registra cliente</title>
</head>
<body>
  <div class="container">
    <div class="col-md-12 p-3">
      <form action="./private/actions/insertClienteAction.php" method="post">
        <!-- Nome e Cognome -->
        <div class="row pb-2">
          <div class="col">
            <label for="nome">Nome</label>
            <input id="nome" type="text" name="nome" class="form-control" placeholder="Nome"value="<?php echo $nome; ?>"/>
          </div>
          <div class="col">
            <label for="cognome">Cognome</label>
            <input id="cognome" type="text" name="cognome"  class="form-control" placeholder="Cognome" value="<?php echo $cognome; ?>"/>
          </div>
        </div>
        <!-- Codice Fiscale -->
        <div class="row pb-2">
          <div class="col">
            <label for="codiceFiscale">Codice fiscale</label>
            <input id="codiceFiscale" type="text" name="codiceFiscale" class="form-control" placeholder="Codice fiscale" value="<?php echo $codiceFiscale; ?>"/>
          </div>
        </div>

        <!-- Email e cellulare -->
        <div class="row pb-2">
          <div class="col">
            <label for="email">Email</label>
            <input  id="email" type="text" name="email" class="form-control" placeholder="Email" value="<?php echo $email; ?>"/>
          </div>
          <div class="col">
            <label for="cellulare">Cellulare</label>
            <input id="cellulare" type="text" name="cellulare" class="form-control" placeholder="Cellulare" value="<?php echo $cellulare; ?>"/>
          </div>
        </div>

        <div class="row pb-2">
          <div class="col">
            <label for="presentatore">Presentato da: </label>
            <input id="presentatore" type="text" name="presentatore" class="form-control" placeholder="Presentatore" value="<?php echo $presentatore; ?>"/>
          </div>
        </div>

        <div class="btn-group">
          <div class="pl-1">
            <input type="reset" class="btn btn-secondary" value="Annulla" />
          </div>
          <div class="float-right">
            <input type="submit" class="btn btn-primary" value="Registra" />
          </div>
        </div>
      </form>
    </div>
  </div>

</body>
</html>
