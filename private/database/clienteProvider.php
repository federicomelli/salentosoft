<?php
require_once 'DBUtils.php';


function insertCliente($codiceFiscale, $nome, $cognome, $email, $cellulare, $presentatore){
	$success=false;
	$insertClienteQuery = "INSERT INTO cliente(cod_fiscale,nome,cognome,email,cellulare,cod_presentatore)VALUES('$codiceFiscale','$nome','$cognome','$email','$cellulare','$presentatore')";

	$mysqli = getConnection();

	if (!$mysqli->query($insertClienteQuery)) {
		echo $mysqli->error;
		$success=false;
	}else{
		$success=true;
	}
	mysqli_close($mysqli);
	return $success;
}

/*
Recupera la lista completa di clienti*/
function getAllClienti(){
	$result = null;
	$selectAllClientiQuery = "SELECT * FROM cliente;";
	$mysqli = getConnection();
	$queryResult = $mysqli->query($selectAllClientiQuery);

	if(!$queryResult->num_rows){
		echo "Non ci sono clienti";
	}else{
		$result=$queryResult;
	}
 	mysqli_close($mysqli);
	return $result;
}

function removeCliente(){

}
/*
Recupera le informazioni di un cliente dato il codice fiscale
*/
function getClienteByCodiceFiscale($codiceFiscale){
	$result = null;
	$selectByCodFisQuery = "SELECT * FROM cliente WHERE cod_fiscale='$codiceFiscale';";
	$mysqli = getConnection();
	$queryResult = $mysqli->query($selectByCodFisQuery);

	if(!$queryResult->num_rows){
		echo "Cliente non trovato";
	}else{
		$result=$queryResult;
	}

	mysqli_close($mysqli);

	return $result;
}

/*
	Aggiorna le informazioni del cliente che ha un determinato codice fiscale
*/
function updateCliente($codiceFiscale, $nome, $cognome, $email, $cellulare, $presentatore){
	$success=false;
	$updateClienteQuery = "UPDATE cliente SET nome='$nome', cognome='$cognome', email='$email', cellulare='$cellulare' WHERE cod_fiscale='$codiceFiscale'";

	$mysqli = getConnection();

	if (!$mysqli->query($updateClienteQuery)) {
		echo $mysqli->error;
		$success=false;
	}else{
		$success=true;
	}
	mysqli_close($mysqli);
	return $success;
}


 ?>
