<?php
require_once (dirname(__FILE__).'/../database/clienteProvider.php');

//flag per controllare l'effettivo successo di un operazione di insert o update
$success = false;

//filter_var - filters a variable with a specified filter
$codiceFiscale = filter_var($_POST['codiceFiscale'], FILTER_SANITIZE_STRING);
$nome = filter_var($_POST['nome'], FILTER_SANITIZE_STRING);
$cognome = filter_var($_POST['cognome'], FILTER_SANITIZE_STRING);
$email = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
$cellulare = filter_var($_POST['cellulare'], FILTER_SANITIZE_STRING);
$presentatore = filter_var($_POST['presentatore'], FILTER_SANITIZE_STRING);

/*
Se il cliente esiste, devo fare un update, altrimenti devo effettuare un insert
*/
if(getClienteByCodiceFiscale($codiceFiscale)){
  //UPDATE
  //TODO - FIX NULL PRESENTATORE
  $success = updateCliente($codiceFiscale, $nome, $cognome, $email, $cellulare, null);
}else{
  //INSERT
  //TODO - FIX NULL PRESENTATORE
  $success = insertCliente($codiceFiscale, $nome, $cognome, $email, $cellulare, null);
};
if($success){
  //SUCCESSO
  //rimuovo la variabile di sessione
  if(isset($_SESSION["cfCliente"])){
    unset($_SESSION["cfCliente"]);
  }
  //redirect alla pagina di visualizzazione dei clienti
  header('HTTP/1.1 301 Moved Permanently');
  header("location: /salentosoft/listaClienti.php");
  die();
}else{
  //ERRORE
  echo '<div class="alert alert-danger" role="alert">Errore</div>';
};

?>
