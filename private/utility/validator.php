<?php

function validateEmail($email){
	$result='';
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		echo "ERRORE";
	}

}

function validateFormCliente($codiceFiscale, $nome, $cognome, $email, $cellulare, $presentatore){
	$codiceFiscale = filter_var($_POST['codiceFiscale'], FILTER_SANITIZE_STRING);
	$nome = filter_var($_POST['nome'], FILTER_SANITIZE_STRING);
	$cognome = filter_var($_POST['cognome'], FILTER_SANITIZE_STRING);
	$email = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
	$cellulare = filter_var($_POST['cellulare'], FILTER_SANITIZE_STRING);
	$presentatore = filter_var($_POST['presentatore'], FILTER_SANITIZE_STRING);

	//todo - inserire validazione
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		echo '<div class="alert alert-danger" role="alert">Errore</div>';
	}
}

 ?>
